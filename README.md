# shanty.nanorc

shanty syntax highlighting for nano

# install

copy the file, adjust the colors as needed, and add the following to `~/.nanorc`:

```
include "path/to/shanty.nanorc"
```

# TT

this project is a Trivial Technology
(see gemini://xj-ix.luxe:1969/wiki/trivial-technology--landing/)
